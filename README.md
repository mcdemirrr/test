# Autocom:

### **NOTE:** Most used files

- application\controllers\Kullanici.php

- application\models\Autocomapi_model.php

- application\models\Fleetmonitor_model.php

- tema\user\assets\app.js

- tema\user\assets\fiyat-alin.js

<br>

### Routing Management

Routing model file path ...application\config\routes.php. The routing system can be accessed here.

<br>

### About user.php>>seo_ata

REQUEST_URI control stages.

- Does REQUEST_URI exist as a page?
- Is REQUEST_URI included as a function in user.php?
- If conditions first and second are not met, an additional check is performed and is REQUEST_URI forwarding included? If it exists, it is redirected(301). If it is not here, it will be redirected to the 404 page.

<br>

### Most used pages;

1) araciniza-fiyat-alin
2) randevu
3) randevu_duzenle

<br>

### About price get your car(araciniza-fiyat-alin)

User vehicle identification operations are performed here.

While defining a vehicle, vehicle information and price are obtained by using Indicata services.

<br>

### Function dependencies

1)pricing_session_control

A function in autocomapi_model (Session control and data set are performed).

Indicata token expiration time and control are performed.

2)start

A function included in autocomapi_model.

It provides connection with Indicata services.

Each time the user defines vehicle information, a request is sent to the indicata. According to the response, the view is transferred.

3)faq_schema

It checks whether there is a faq schema usage or not. If there is, the faq schema script is used.

<br>

### About fiyat-alin.js functions

<br>

**on('click','[data-price-value]')**

Allows selection of vehicle information.

With the help of ajax, the price-get-form in user.php is used.

<br>

**on('click','[data-remove-step]')**

It allows the user to go back in users selection.

<br>

**on('submit','#complete-form,#phone-approve-form,#phone-approve-code-form')**

Example:
My agent values form(ajax url): action>>https://* domain */save-priyat-alin-form

NOTE: JS>>save-price-get-form User.php>>save_price_get_form

<br>

### About save_price_get_form function

<br>

Each information in the form goes through form_validation.

Then, data(postfields) are passed to createSession() in autocomApi_model.

The session format is created.

The getPricingSettings() function checks the criteria(min_purchase_price_limit,price_display_limit etc ).

Car price is calculated using indicata services in getVaulationPrice() function.

If an error is received in pricing, the pricing session is unset.

Criteria control is performed on the entered car information(min_year, max_year, km).

If the price is returned to the car, the price is broken down for some vehicles with the transmissionControl() function.

The transmissionControl() function revises the price of certain vehicles at the rate determined.

min_purchase_price_limit,,max_purchase_price_limit,price_display_limit information is compared with the specified criteria.

If the vehicle is not out of criteria, a notification e-mail is sent to the user.

Users are forwarded to IYS with the information obtained from the form.

Finally, the data is transmitted to FM. fleet_monitor() is used.

<br>

**About fleet_monitor function**

The vehicle card is created. Vehicle ID taken from vehicle card.

The vehicle is updated so that additional vehicle information can be transferred.

The returned value of updated vehicles result is transferred.

<br>

### About car price thanks(araciniza-fiyat-tesekkurler)

The user is redirected to this page after receiving an offer to his/her car

The user can switch to the appointment page from this page.

<br>

### About appointment page(randevu)

There are 4 stages on the appointment pages. 4 sayfa arasındaki temel bağlantı randevu() fonksiyonunda sağlanır.Sessiondaki bilgiler kontrol edilerek yonlendirmeler gerçekleştirilir.

1) Appointment location selection

getDealersFromApi is called. getDealersFromApi response also returns the locations found in simplyBook.me. Returned locations are passed to findDealersFromIds function in ...appointment.php, findDealersFromIds function searches for locations in $dealers in Autocomapi_model and matching locations are transferred to the interface.

After the appointment location is selected, the expert-point-sec function is called with ajax. expert-point-sec allows the id of the selected location to be registered to the session.

2) Appointment date and time selection

3 different simplyBook.me web services are used. These are getWorkCalendar, startTimeMatrix and getTimelineSlots. These functions are called using the appointment-date.js file.

**getWorkCalendar** allows to determine the availability of days. For each request, a time frame of 1 month is returned in the response. With the information received from the response, the availability status of the days is transferred to the interface.

**getTimelineSlots** allows us to analyze the availability of days, whether they are full or empty.

**startTimeMatrix** transmits available time slots for the requested day.

If the date and time are selected and confirmed, the date and time information is sent to the **set-book-date-hour** function. set-book-date-hour allows the selected date information to be recorded to the session.

3) Phone verification

Two different Twilio web services are used. These are **sendSmsVerificationRequest** and **smsVerificationRequest** .

With the Verification Code Sending operation, the redirection to the set_book_phone function is performed. The set_book_phone function validates the entered phone number. After validation, sendSmsVerificationRequest function is called. The sendSmsVerificationRequest function transmits a verification code to the entered phone number.

After the verification code is transmitted, the verification code entry screen is reflected on the interface. After the verification code is entered and approved, smsVerificationRequest is called. The code entered by the user is verified with the help of smsVerificationRequest.

4) Appointment Result

The location, date and time information that the user has selected is displayed on the interface.

If confirmed, it is redirected to the accept_book function.

The accept_book function creates an appointment in simplyBook.me, the vehicle card is updated on the fleet monitor and the user information is transferred.

**About making an appointment on simpleBook.me:**

The user is queried with getClientList.

User is updated with editClient.

A new user is created with addClient.

With the book function, an appointment is created at simplyBook.me.

**About Fleet monitor:**

After the appointment confirmation, the fleet_monitor_booking function is used.

fleet_monitor_booking is used to transfer user and appointment information.

In fleet monitor, users are singularized according to their phone numbers. To preserve singularity, users are first queried with getCustomer when importing to fleet monitor. If the user is not found, it is created with the createCustomer function. If it is found, the information is updated with the updateCustomer.

In the process of transferring the appointment information, the carId is obtained with the searchVehicles function. With this information, the vehicle card is updated with the putVechile function.

<br>

### About successful appointment(appointment-successful)

There are appointment details, important reminders and purchase conditions on the page.

All of the information contained is printed with the data in the session.

<br>

### About edit appointment

On the appointment edit page, the user can update or cancel his appointment.

The code in the appointment editing link is the encrypted version of the contract number. This code is encrypted and the contract number is obtained.

**About appointment update**


There are 3 stages on the appointment pages. The basic connection between the 3 pages is provided in the appointment_arrange() function. The information in the session is checked and the directions are made.

Unencrypted code is passed to the checkBookCode function. The code is encrypted within the checkBookCode function and written to the session. The contract number is used in the searchVehicles function to obtain the carId. With CarId, vehicle information and sbm_id(simplyBook.me id) are obtained. A request is made to the getBookingDetails function with sbm_id. The information returned in the response is written to the session. The information in Session is used in the interface.

1) Appointment edit location selection

getDealersFromApi is called. getDealersFromApi response also returns the locations found in simplyBook.me. Returned locations are passed to in ...appointment.php, findDealersFromIds function searches for locations in $dealers in Autocomapi_model and matching locations are transferred to the interface.

After the appointment location is selected, the expert-point-edit function is called with ajax. It allows the id of the selected location to be registered to the session by expert-point-order.

2) Edit appointment, date and time selection

3 different simplyBook.me web services are used. These are getWorkCalendar, startTimeMatrix and getTimelineSlots. These functions are called using the appointment-date.js file.

**getWorkCalendar** allows to determine the availability of days. For each request, a time frame of 1 month is returned in the response. With the information received from the response, the availability status of the days is transferred to the interface.

**getTimelineSlots** allows us to analyze the availability of days, whether they are full or empty.

**startTimeMatrix** transmits available time slots for the requested day.

If the date and time are selected and confirmed, the date and time information is sent to the **edit-book-date-hour** function. edit-book-date-hour allows the selected date information to be recorded in the session.

3) Arrange an appointment result

The location, date and time information that the user has selected is displayed on the interface.

If approved, edit_accept_book is redirected to the function.

edit_accept_book function creates appointment in simplyBook.me, vehicle card is updated in fleet monitor and user information is transferred.

The carId is obtained with the searchVehicles function in edit_accept_book. In the getVechile function, the vehicle, user and sbmId information are obtained by using carId, and then the session is printed.

The sbmId obtained from the Fleet monitor is used in the getBookingDetails function. GetBookingDetails to get user information

**About update appointment on simpleBook.me:**

An appointment comment is added with setBookingComment.

User is updated with editClient.

With the editBook function the appointment at simplyBook.me is updated.

**About Fleet monitor:**

After the appointment confirmation, the fleet_monitor_edit_booking function is used.

fleet_monitor_edit_booking is used to update user and appointment information.

